/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clear_tetri.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <lcuzzuco@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/05 15:40:52 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/06/05 15:40:54 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	clear_tetri(t_config *config, int tetri_index)
{
	int		i;
	int		j;
	int		count;

	count = 0;
	i = 0;
	config->tetris[tetri_index].is_placed = 0;
	config->all_tetris_placed = 0;
	while (i < GRID_SIZE)
	{
		j = 0;
		while (j < GRID_SIZE)
		{
			if (config->grid[i][j] == 'A' + tetri_index)
			{
				config->grid[i][j] = '.';
				count++;
				if (count == 4)
					return ;
			}
			j++;
		}
		i++;
	}
}
