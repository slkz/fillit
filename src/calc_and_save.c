/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calc_and_save.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <lcuzzuco@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/05 15:40:45 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/06/05 15:40:49 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		calc_gridlen(char **grid)
{
	int		i;
	int		j;
	int		len;

	i = 0;
	len = 0;
	while (i < GRID_SIZE)
	{
		j = 0;
		while (j < GRID_SIZE)
		{
			if (grid[i][j] != '.')
			{
				if (i > len)
					len = i;
				if (j > len)
					len = j;
			}
			j++;
		}
		i++;
	}
	return (len + 1);
}

void	calc_and_save(t_config *config)
{
	int		i;

	i = 0;
	if (calc_gridlen(config->grid) < config->result_len)
	{
		config->result_len = calc_gridlen(config->grid);
		while (i < GRID_SIZE)
		{
			ft_memcpy(config->result[i], config->grid[i], GRID_SIZE);
			i++;
		}
	}
}
