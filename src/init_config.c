/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_config.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <lcuzzuco@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/05 15:40:58 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/06/05 15:41:01 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static void		init_tetri(t_config *config)
{
	int		i;

	i = 0;
	while (i < 26)
	{
		config->tetris[i].data = NULL;
		config->tetris[i].is_placed = 0;
		i++;
	}
}

void			init_config(t_config *config)
{
	config->grid = NULL;
	config->result = NULL;
	config->item_count = 0;
	init_tetri(config);
	config->result_len = GRID_SIZE - 1;
	config->all_tetris_placed = 0;
	config->skip = 0;
}
