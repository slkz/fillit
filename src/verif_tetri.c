/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   verif_tetri.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpinault <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 18:46:50 by rpinault          #+#    #+#             */
/*   Updated: 2017/06/06 18:46:51 by rpinault         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	try_to_place_tetri(t_config *config, int y, int x, int tetri_index)
{
	static int	i;
	static int	j;

	i = 0;
	while (i < 4)
	{
		j = 0;
		while (j < 4)
		{
			if (config->tetris[tetri_index].data[i][j] == '#')
			{
				if (config->grid[y + i][x + j] != '.')
					return ;
			}
			j++;
		}
		i++;
	}
	next_call(config, y, x, tetri_index);
}

void	try_to_place_opti(t_config *config, int tetri_index)
{
	static int	max;
	int			x;
	int			y;

	y = 0;
	max = config->max_try;
	while (y < max)
	{
		x = 0;
		while (x < max)
		{
			try_to_place_tetri(config, y, x, tetri_index);
			if (config->skip)
				break ;
			x++;
		}
		y++;
	}
}
