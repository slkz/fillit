/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   adjust_tetri.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <lcuzzuco@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/05 15:40:25 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/06/05 15:40:28 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	moove_left(char **tetri)
{
	int		i;

	i = 0;
	while (i < 4)
	{
		tetri[i][0] = tetri[i][1];
		tetri[i][1] = tetri[i][2];
		tetri[i][2] = tetri[i][3];
		tetri[i][3] = '.';
		i++;
	}
}

void	moove_up(char **tetri)
{
	ft_memcpy(tetri[0], tetri[1], 4);
	ft_memcpy(tetri[1], tetri[2], 4);
	ft_memcpy(tetri[2], tetri[3], 4);
	ft_memset(tetri[3], '.', 4);
}

int		check_line(char **tetri)
{
	int		i;

	i = 0;
	while (i < 4)
	{
		if (tetri[0][i] == '#')
			return (1);
		i++;
	}
	return (0);
}

int		check_column(char **tetri)
{
	int		i;

	i = 0;
	while (i < 4)
	{
		if (tetri[i][0] == '#')
			return (1);
		i++;
	}
	return (0);
}

void	adjust_tetri(char **tetri)
{
	while (check_line(tetri) == 0)
		moove_up(tetri);
	while (check_column(tetri) == 0)
		moove_left(tetri);
}
