/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit_typedefs.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <lcuzzuco@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/05 15:39:25 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/06/05 15:39:30 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_TYPEDEFS_H
# define FILLIT_TYPEDEFS_H

/*
** grid: la plus petite grille
** item_tab: tableau de tetriminos (tableau de 26 char**)
** item_count: nombre de tetriminos
*/

typedef struct		s_tetri
{
	char			**data;
	int				is_placed;
}					t_tetri;

typedef struct		s_config
{
	char	**grid;
	char	**result;
	t_tetri	tetris[26];
	int		item_count;
	int		result_len;
	int		all_tetris_placed;
	int		solve_iter;
	int		skip;
	int		max_try;
}					t_config;

#endif
