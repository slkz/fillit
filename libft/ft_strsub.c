/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 19:05:17 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/04/10 19:05:37 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char			*new;
	size_t			count;
	unsigned int	index;

	if (!s)
		return (0);
	index = start;
	count = 0;
	new = malloc(sizeof(char) * (len + 1));
	if (!new)
		return (NULL);
	while (count < len)
	{
		new[count] = s[index];
		index++;
		count++;
	}
	new[count] = '\0';
	return (new);
}
