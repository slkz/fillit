/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   permute.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpinault <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 18:45:54 by rpinault          #+#    #+#             */
/*   Updated: 2017/06/06 18:45:56 by rpinault         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "fillit.h"

void	swap(int *v, const int i, const int j)
{
	int	t;

	t = v[i];
	v[i] = v[j];
	v[j] = t;
}

void	rotate_left(int *v, const int start, const int n)
{
	int	tmp;
	int	i;

	tmp = v[start];
	i = start;
	while (i < n - 1)
	{
		v[i] = v[i + 1];
		i++;
	}
	v[n - 1] = tmp;
}

void	display_permut(int *tab, int len)
{
	int		i;

	i = -1;
	while (++i < len)
		printf("%d", tab[i]);
	printf("\n");
}

void	permute(int *combi, const int start, t_config *config)
{
	const int	len = config->item_count;
	int			i;
	int			j;

	display_permut(config->combi, config->item_count);
	solve(combi, 0, config);
	if (start < len)
	{
		i = len - 2;
		while (i >= start)
		{
			j = i + 1;
			while (j < len)
			{
				swap(combi, i, j);
				permute(combi, i + 1, config);
				j++;
			}
			rotate_left(combi, i, len);
			i--;
		}
	}
}

void	init_combinations(t_config *config)
{
	int		i;
	int		len;

	i = 0;
	len = config->item_count;
	if ((config->combi = malloc(sizeof(int) * len)) == NULL)
		exit_routine();
	while (i < len)
	{
		config->combi[i] = i;
		i++;
	}
}
