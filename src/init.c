/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <lcuzzuco@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/05 15:41:05 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/06/05 15:41:07 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static int	check_file_retval(t_config *conf, char *b, int i, int nb_lines)
{
	if (b[i] == '\0' && nb_lines == 4 \
			&& (conf->tetris[conf->item_count++].data = ft_strsplit(b, '\n')))
		return (1);
	else
		return (0);
}

static int	check_file(t_config *conf, char *b)
{
	int	i;
	int	nb_lines;

	i = -1;
	nb_lines = 0;
	while (b[++i] && nb_lines != 4 \
			&& (b[i] == '.' || b[i] == '#' || b[i] == '\n'))
	{
		if (b[i] == '\n')
		{
			if ((i % 5) == 4)
				nb_lines++;
			else
				return (0);
		}
	}
	if (b[i] == '\n' && nb_lines == 4)
	{
		b[i++] = '\0';
		conf->tetris[conf->item_count++].data = ft_strsplit(b, '\n');
		return (b[i] == '.' || b[i] == '#' ? check_file(conf, &(b[i])) : 0);
	}
	return (check_file_retval(conf, b, i, nb_lines));
}

int			init(t_config *config, char *filename)
{
	int		fd;
	int		len;
	char	buffer[BUF_SIZE];

	if ((fd = open(filename, O_RDONLY)) == -1)
		return (0);
	if ((len = read(fd, buffer, BUF_SIZE - 1)) < 0)
		return (0);
	buffer[len] = '\0';
	init_config(config);
	if (!check_file(config, buffer) || !check_tetriminos(config))
		return (0);
	config->max_try = 4 * config->item_count;
	return (1);
}
