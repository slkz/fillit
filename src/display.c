/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   display.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpinault <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 18:45:46 by rpinault          #+#    #+#             */
/*   Updated: 2017/06/06 18:45:47 by rpinault         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	fillit_display(t_config *config)
{
	int		i;

	i = 0;
	while (i < config->result_len)
	{
		write(1, config->result[i], config->result_len);
		ft_putchar('\n');
		i++;
	}
}

void	display_grid(char **grid)
{
	int		i;
	int		j;

	i = 0;
	while (i < GRID_SIZE)
	{
		j = 0;
		while (j < GRID_SIZE)
		{
			ft_putchar(grid[i][j]);
			j++;
		}
		ft_putchar('\n');
		i++;
	}
}
