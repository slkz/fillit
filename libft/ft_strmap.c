/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/10 19:03:39 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/04/10 19:05:36 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*str;
	char	*clean;
	int		i;

	i = 0;
	str = (char *)s;
	if (str == NULL)
		return (NULL);
	if ((clean = malloc(sizeof(char) * ft_strlen(str) + 1)) == NULL)
		return (NULL);
	while (s[i] != '\0' && f)
	{
		clean[i] = f(s[i]);
		i++;
	}
	clean[i] = '\0';
	return (clean);
}
