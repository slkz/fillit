/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpinault <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 18:46:29 by rpinault          #+#    #+#             */
/*   Updated: 2017/06/09 20:16:05 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int		display(t_config *config)
{
	int		i;
	int		j;
	int		k;

	i = -1;
	while (++i < config->item_count)
	{
		j = -1;
		while (++j < 4)
		{
			k = -1;
			while (config->tetris[i].data[j][++k])
			{
				if (config->tetris[i].data[j][k] == '#')
					ft_putchar('A' + i);
				else
					ft_putchar(config->tetris[i].data[j][k]);
			}
			ft_putchar('\n');
		}
		ft_putchar('\n');
	}
	return (0);
}

void	init_tab(char ***tab_addr, char c, int x, int y)
{
	int		i;
	int		j;

	i = 0;
	if ((*tab_addr = malloc(sizeof(char *) * y)) == NULL)
		return ;
	while (i < y)
	{
		if (((*tab_addr)[i] = malloc(sizeof(char) * x)) == NULL)
			return ;
		j = 0;
		while (j < x)
		{
			(*tab_addr)[i][j] = c;
			j++;
		}
		i++;
	}
}

int		main(int argc, char **argv)
{
	t_config		config;

	if (argc != 2)
	{
		ft_putstr("usage: ./fillit grid_file\n");
		return (-1);
	}
	if (!init(&config, argv[1]))
	{
		ft_putstr_fd("error\n", STDOUT_FILENO);
		return (-1);
	}
	init_tab(&config.grid, '.', GRID_SIZE, GRID_SIZE);
	init_tab(&config.result, '.', GRID_SIZE, GRID_SIZE);
	solve(&config, 0);
	fillit_display(&config);
	return (0);
}
