/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   solve.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <lcuzzuco@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/05 15:41:11 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/06/05 15:41:20 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	next_call(t_config *config, int y, int x, int tetri_index)
{
	int		nb;
	int		save_skip;

	nb = 0;
	save_skip = config->skip;
	write_tetri(config, y, x, tetri_index);
	if (!config->skip && calc_gridlen(config->grid) >= config->result_len)
		config->skip = 1;
	while (nb < config->item_count)
	{
		if (config->tetris[nb].is_placed != 1)
			break ;
		nb++;
	}
	config->all_tetris_placed = (nb == config->item_count) ? 1 : 0;
	solve(config, tetri_index + 1);
	config->skip = save_skip;
	clear_tetri(config, tetri_index);
}

void	solve(t_config *config, int tetri_index)
{
	if (config->skip == 1)
		return ;
	if (config->all_tetris_placed == 1)
	{
		calc_and_save(config);
		config->all_tetris_placed = 0;
		return ;
	}
	try_to_place_opti(config, tetri_index);
}
