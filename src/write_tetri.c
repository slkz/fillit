/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   write_tetri.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpinault <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 18:46:41 by rpinault          #+#    #+#             */
/*   Updated: 2017/06/06 18:46:42 by rpinault         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "fillit.h"

void	write_tetri(t_config *config, int y, int x, int tetri_index)
{
	int		i;
	int		j;

	config->tetris[tetri_index].is_placed = 1;
	i = 0;
	while (i < 4)
	{
		j = 0;
		while (j < 4)
		{
			if (config->tetris[tetri_index].data[i][j] == '#')
				config->grid[y + i][x + j] = 'A' + tetri_index;
			j++;
		}
		i++;
	}
}
