
#--------------------------------- Makefile -----------------------------------#

# Executable name
NAME = fillit

# Project layout
SRC_DIR = ./src/
INC_DIR = ./includes/
OBJ_DIR = ./obj/

# Compiler and linker settings
CC = clang
CFLAGS = -Wall -Wextra -Werror
LFLAGS = -L./libft/ -lft
IFLAGS = -I$(INC_DIR) -I./libft

# Extra libraries
LIBFT = libft/libft.a

# Project files
SRC_FILES = main.c \
			display.c \
			init.c \
			solve.c \
			adjust_tetri.c \
			calc_and_save.c \
			verif_tetri.c \
			write_tetri.c \
			clear_tetri.c \
			exit.c \
			check_adjacente_places.c \
			init_config.c \
			check_tetri.c

OBJ_FILES = $(SRC_FILES:.c=.o)
INC_FILES = fillit.h

# List of files
SRC = $(addprefix $(SRC_DIR), $(SRC_FILES))
OBJ = $(addprefix $(OBJ_DIR), $(OBJ_FILES))
INC = $(addprefix $(INC_DIR), $(INC_FILES))

.PHONY: all clean fclean re

#------------------------------------------------------------------------------#

all: $(OBJ_DIR) $(LIBFT) $(NAME) 

$(OBJ_DIR):
	@mkdir -p $(OBJ_DIR)

$(NAME): $(OBJ)
	@$(CC) -o $@ $^ $(LFLAGS)

$(LIBFT):
	@make -C libft/

$(OBJ_DIR)%.o: $(SRC_DIR)%.c $(INC)
	$(CC) $(CFLAGS) $(IFLAGS) -o $@ -c $<

clean:
	@make -C libft/ clean
	@rm -rf $(OBJ_DIR)

fclean: clean
	@rm -f $(NAME) $(LIBFT)

re: fclean all
