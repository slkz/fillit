/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <lcuzzuco@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/05 15:39:16 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/06/05 15:39:21 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FILLIT_H
# define FILLIT_H

# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <unistd.h>

# include <stdlib.h>

# include "fillit_typedefs.h"
# include "../libft/libft.h"

# define BUF_SIZE 1024
# define GRID_SIZE 52

/*
** init.c
*/

int		init(t_config *config, char *filename);

/*
** display.c
*/

void	fillit_display(t_config *config);

/*
** solve.c
*/

void	solve(t_config *config, int tetri_index);
void	solve_opti(t_config *config, int tetri_index);

void	adjust_tetri(char **tetri);

void	calc_and_save(t_config *config);
void	clear_tetri(t_config *config, int tetri_index);
void	write_tetri(t_config *config, int y, int x, int tetri_index);
void	try_to_place_tetri(t_config *config, int y, int x, int tetri_index);
int		calc_gridlen(char **grid);

void	exit_routine(void);
void	place_tetri(t_config *config, int tetri_index);
void	init_config(t_config *config);
void	display_grid(char **grid);
void	next_call(t_config *config, int y, int x, int tetri_index);
void	try_to_place_opti(t_config *config, int tetri_index);
int		check_tetriminos(t_config *config);

#endif
