/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_tetri.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lucuzzuc <lcuzzuco@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/05 15:40:37 by lucuzzuc          #+#    #+#             */
/*   Updated: 2017/06/05 15:40:40 by lucuzzuc         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static void		reset_diez(char **tetri)
{
	int	i;
	int	j;

	i = 0;
	while (i < 4)
	{
		j = 0;
		while (tetri[i][j])
		{
			if (tetri[i][j] == '*')
				tetri[i][j] = '#';
			j++;
		}
		i++;
	}
}

static int		recursive_check(char **tetri, int i, int j, int *diez)
{
	(*diez)++;
	tetri[i][j] = '*';
	if (i < 3 && tetri[i + 1][j] == '#')
		recursive_check(tetri, i + 1, j, diez);
	if (j < 3 && tetri[i][j + 1] == '#')
		recursive_check(tetri, i, j + 1, diez);
	if (i > 0 && tetri[i - 1][j] == '#')
		recursive_check(tetri, i - 1, j, diez);
	if (j > 0 && tetri[i][j - 1] == '#')
		recursive_check(tetri, i, j - 1, diez);
	return (*diez == 4 ? 1 : 0);
}

static int		verif_adjacent_diez(char **tetri)
{
	int	i;
	int	j;
	int	diez;

	i = 0;
	diez = 0;
	while (i < 4)
	{
		j = 0;
		while (tetri[i][j])
		{
			if (tetri[i][j] == '#')
				return (recursive_check(tetri, i, j, &diez));
			j++;
		}
		i++;
	}
	return (0);
}

static int		count_diez(char **tetri)
{
	int	i;
	int	j;
	int	diez;

	i = 0;
	diez = 0;
	while (i < 4)
	{
		j = 0;
		while (tetri[i][j])
		{
			if (tetri[i][j] == '#')
				diez++;
			j++;
		}
		i++;
	}
	return (diez == 4 ? 1 : 0);
}

int				check_tetriminos(t_config *config)
{
	int	i;

	i = 0;
	while (i < config->item_count)
	{
		if (!count_diez(config->tetris[i].data)
				|| !verif_adjacent_diez(config->tetris[i].data))
			return (0);
		reset_diez(config->tetris[i].data);
		adjust_tetri(config->tetris[i].data);
		i++;
	}
	return (1);
}
