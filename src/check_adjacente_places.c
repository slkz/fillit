/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_adjacente_places.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rpinault <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/06/06 18:45:29 by rpinault          #+#    #+#             */
/*   Updated: 2017/06/06 18:45:31 by rpinault         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

void	place_tetri(t_config *config, int tetri_index)
{
	int		i;
	int		j;

	i = 0;
	while (i < config->max_try)
	{
		j = 0;
		while (j < config->max_try)
		{
			try_to_place_tetri(config, i, j, tetri_index);
			j++;
		}
		i++;
	}
}
